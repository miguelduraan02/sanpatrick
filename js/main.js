// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.12.2/firebase-app.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
import { getDatabase ,onValue, ref as refS, set, child, get, update, remove } 
from"https://www.gstatic.com/firebasejs/10.12.2/firebase-database.js";

const firebaseConfig = {

  apiKey: "AIzaSyC24UUGDnHCjahBhbbzQogYb2U_A2DdR8E",
  authDomain: "proyectofinalm-5d830.firebaseapp.com",
  databaseURL: "https://proyectofinalm-5d830-default-rtdb.firebaseio.com",
  projectId: "proyectofinalm-5d830",
  storageBucket: "proyectofinalm-5d830.appspot.com",
  messagingSenderId: "607530527638",
  appId: "1:607530527638:web:c6fd91e0ec689590cacbb7"
};

  // Initialize Firebase
    const app = initializeApp(firebaseConfig);
    const db = getDatabase(app);