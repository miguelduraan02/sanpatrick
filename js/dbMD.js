// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.12.2/firebase-app.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

import { getDatabase ,onValue, ref as refS, set, child, get, update, remove } 
from "https://www.gstatic.com/firebasejs/10.12.2/firebase-database.js";

import { getStorage, ref, uploadBytesResumable, getDownloadURL }
  from "https://www.gstatic.com/firebasejs/10.12.2/firebase-storage.js";

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCd7enP2s-uNn59eKEMYoWYuM94UAklCQk",
  authDomain: "racing-palace.firebaseapp.com",
  databaseURL: "https://racing-palace-default-rtdb.firebaseio.com",
  projectId: "racing-palace",
  storageBucket: "racing-palace.appspot.com",
  messagingSenderId: "395981662071",
  appId: "1:395981662071:web:bad2f0d45075a184ffa372"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
const storage = getStorage();

//declarar unas Variables global
var numCodigo = 0;
var categoria = "";
var nombreL = "";
var precio = 0;
var urlImag = "";

// Listar Productos

Listarproductos();

function Listarproductos() {
    const section = document.getElementById("sectProductos");

    const dbref = refS(db, 'Productos');


    onValue(dbref, (snapshot) => {
        const productosPorCategoria = {};

        snapshot.forEach(childSnapshot => {
            const data = childSnapshot.val();
            const categoria = data.categoria;

            if (!productosPorCategoria[categoria]) {
                productosPorCategoria[categoria] = [];
            }
            productosPorCategoria[categoria].push(data);
        });


        section.innerHTML = '';

        for (const [categoria, productos] of Object.entries(productosPorCategoria)) {
            section.innerHTML += `<hr><h2>${categoria}</h2><hr>`;

            productos.forEach(producto => {
                section.innerHTML += `<div class='card'>
                    <img id='urlImag' src='${producto.urlImag}' alt='' width='100'>
                    <p id='nombreL' class='anta-regurar'>${producto.nombreL}</p>
                    <p id='precio'>${producto.precio}</p>
                    <button>Agregar al carrito</button>
                </div>`;
            });
        }

    }, {onlyOnce : true});

}